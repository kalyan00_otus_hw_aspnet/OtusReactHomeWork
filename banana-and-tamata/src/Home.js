import React, { useState } from 'react';
import { Button, Toast } from 'react-bootstrap';


export default function Home() {
    return (
        <>
            <h1 className="header">Welcome To React-Bootstrap</h1>
            <ExampleToast>
                We now have Toasts
                <span role="img" aria-label="tada">
                    🎉
                </span>
            </ExampleToast>
        </>
    );
}



const ExampleToast = ({ children }) => {
    const [show, toggleShow] = useState(true);

    return (
        <>
            {!show && <Button onClick={() => toggleShow(true)}>Show Toast</Button>}
            <Toast show={show} onClose={() => toggleShow(false)}>
                <Toast.Header>
                    <strong className="mr-auto">React-Bootstrap</strong>
                </Toast.Header>
                <Toast.Body>{children}</Toast.Body>
            </Toast>
        </>
    );
};
