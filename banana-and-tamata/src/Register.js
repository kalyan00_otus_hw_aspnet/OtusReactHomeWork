import { Button, Form } from 'react-bootstrap';
import React, { useState } from 'react';
import axios from 'axios';



export default function Register() {

    const [email, setEmail] = useState(null);
    const [login, setLogin] = useState(null);
    const [password, setPassword] = useState(null);

    function WrapTarget(func) {
        return e => func(e.target.value);
    }

    function Registered(login) {

    }

    function ApiRegister() {
        axios({
            method: "PUT",
            url: "/api/v1/Identity?login=" + (login) + "&password=" + (password) + "&email=" + (email)
        }).then(res => Registered(res.data.login));

    }


    return (
        <Form>
            <h2>Wellcome to Registration</h2>
            <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" onChange={WrapTarget(setEmail)} />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" onChange={WrapTarget(setPassword)}/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicLogin">
                <Form.Label>Login</Form.Label>
                <Form.Control type="login" placeholder="Login" onChange={WrapTarget(setLogin)} />
            </Form.Group>

            <Button variant="primary" onClick={ApiRegister}>
                Submit
            </Button>
        </Form>);
}
