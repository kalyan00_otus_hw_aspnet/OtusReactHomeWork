import './App.css';

import React, { useEffect, useState} from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { Route, Routes, useNavigate  } from 'react-router-dom';
import { Person } from 'react-bootstrap-icons';
import { useDispatch, useSelector } from 'react-redux';
import Login from './Login';
import Home from './Home';
import NotFound from './NotFound';
import Register from './Register';



export default function App() {
    const dispatch = useDispatch();
    const login = useSelector(state => state.login);
    const navigate = useNavigate();
    const [location, setLocation] = useState(null);

    function Logout() {
        dispatch({ type: "LOGOUT", payload: null })
    };

    useEffect(() => {
        if (location) {
            navigate(location);
        }
        // eslint-disable-next-line
    }, [location]);

    return <>
        <Navbar bg="light" expand="lg">
            <Container>
                <Nav className="me-auto">
                    <Nav.Link onClick={()=>setLocation("/")}>Home</Nav.Link>
                    {
                        (login === null) ? (
                            <>
                                <Nav.Link onClick={() => setLocation("/login")}>Login</Nav.Link>
                                <Nav.Link onClick={() => setLocation("/register")}>Register</Nav.Link>
                            </>
                        ) : (
                            <NavDropdown title={<><Person />{login}</>} id="basic-nav-dropdown">
                                <NavDropdown.Item onClick={Logout}>Logout </NavDropdown.Item>
                            </NavDropdown>

                        )

                    }
                </Nav>
            </Container>
        </Navbar>
        <Container className="p-3">
            <Container className="p-5 mb-4 bg-light rounded-3">

                <Routes>
                    <Route exact path="/" element={<Home />} />
                    <Route exact path="/register" element={<Register />} />
                    <Route exact path="/login" element={<Login />} />
                    <Route path="*" element={<NotFound />} />
                </Routes>
            </Container>
        </Container>
    </>;
}

