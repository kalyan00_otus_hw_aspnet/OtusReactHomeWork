import axios from "axios";
import React, { useState } from "react";
import { useDispatch } from 'react-redux';

function Login() {
    const [loginMsg, setLoginMsg] = useState(null);
    const [login, setLogin] = useState(null);
    const [password, setPassword] = useState(null);

    const dispatch = useDispatch();


    function logininternal() {
        axios({
            method: "POST",
            url: "/api/v1/Identity?login=" + (login) + "&password=" + (password)
        }).then(res => {
            const login = res.data.login;
            dispatch({
                type: "LOGIN",
                payload: login,
            })
            setLoginMsg(login)
        });


    }

    if (loginMsg == null)
        return (
            <>
                <p><b>Login</b><br />
                    <input type="text" size="40" onInput={e => setLogin(e.target.value)} />
                </p>
                <p><b>Password</b><br />
                    <input type="text" size="40" onInput={e => setPassword(e.target.value)} />
                </p>
                <button onClick={logininternal} > login </button>
            </>
        );
    return (
        <>
            <p>Welcome {loginMsg}</p>
        </>
    );

}

export default Login;
