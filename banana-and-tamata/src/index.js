import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';


const defaultState = {
    login: "sadf",
}
const LoginActions = {
    LOGIN: "LOGIN",
    LOGOUT: "LOGOUT",
};
const reducer = (state = defaultState, action) => {
    switch (action.type) {
        case LoginActions.LOGIN:
            return { ...state, login: action.payload }
        case LoginActions.LOGOUT:
            return { ...state, login: null };
        default:
            return state;
    }
}

const store = createStore(reducer);



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </Provider>
    </React.StrictMode>
);
