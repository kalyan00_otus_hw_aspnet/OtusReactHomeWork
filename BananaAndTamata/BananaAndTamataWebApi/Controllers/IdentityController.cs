using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BananaAndTamataWebApi.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class IdentityController : ControllerBase
    {
       
        private readonly ILogger<IdentityController> _logger;

        public IdentityController(ILogger<IdentityController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public async Task<ActionResult> Login(string login, string password)
        {
            _logger.LogDebug(login);
            if (login == "none")
                return NotFound();
            return Ok(new { login });
        }
        [HttpPut]
        public async Task<ActionResult> Register(string login, string password, string email)
        {
            _logger.LogDebug(login);
            if (login == "none")
                return NotFound();
            return Ok(new { login });
        }

        [HttpGet]
        public async Task<ActionResult> Ping()
        {
            return Ok("Success "+DateTime.Now );
        }
    }
}